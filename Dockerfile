ARG VERSION=latest

FROM gitlab/glab:$VERSION

RUN wget https://github.com/jqlang/jq/releases/latest/download/jq-linux-amd64 \
 && chmod +x jq-linux-amd64 \
 && mv jq-linux-amd64 /usr/bin/jq

RUN apk add --no-cache py3-pip npm bash \
 && pip install --no-cache-dir --break-system-packages pre-commit
